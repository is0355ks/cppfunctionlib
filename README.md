# README #

Nodeで利用する関数型ライブラリをC++で書いたテスト用のコードです。  
Markdown初めて書いたのでこの文章も見辛いですごめんなさい  
現状 each と map の実装ができてます(たぶん)

## いじったファイル ##
### ./app.js ###
C++で書いたモジュールを読み込んで実行する用のJS

### ./main.js ###
JSで書いた同じ動作をするモジュール(速度比較用)を読み込んで実行する用のJS  
まだeachもmapもいれてないです

### ./Module/func.cc ###
C++で書いた一番メインのコード  
V8仕様なのでとてもとても見辛い分からない



## 参考 ##
- http://qiita.com/hiropanda/items/7b7d6843b8a721f25034
- http://underscorejs.org/
- https://nodejs.org/api/addons.html
- http://bespin.cz/~ondras/html/index.html
- http://d.hatena.ne.jp/hagino_3000/20120320/1332228809