#include <node.h>
#include <cstring>

namespace caesar {

  using namespace v8;

  void Caesar(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate =args.GetIsolate();
    HandleScope scope(isolate);

    int key = args[1]->NumberValue();
    String::Utf8Value str(args[0]->ToString());
    char *pstr;
    pstr = str.operator*();

    for (int i = 0; i < str.length(); i++ ){
      pstr[i] = pstr[i] + key;
    }
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, pstr));
  }

  void Decaesar(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate =args.GetIsolate();
    HandleScope scope(isolate);

    int key = args[1]->NumberValue();
    String::Utf8Value str(args[0]->ToString());
    char *pstr;
    pstr = str.operator*();

    for (int i = 0; i < str.length(); i++ ){
      pstr[i] = pstr[i] - key;
    }
  args.GetReturnValue().Set(String::NewFromUtf8(isolate, pstr));
  }
  void init(v8::Local<v8::Object> exports) {
    NODE_SET_METHOD(exports, "caesar", Caesar);
    NODE_SET_METHOD(exports, "decaesar", Decaesar);
  }

  NODE_MODULE(addon, init)
}
