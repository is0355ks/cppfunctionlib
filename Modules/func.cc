#include <node.h>

namespace func {

  using namespace v8;

  void Each(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);

    Local<Array> arr = Local<Array>::Cast(args[0]);
    int len = arr -> Length();
    Local<Function> cb = Local<Function>::Cast(args[1]);
    const unsigned argc = 1;
    for(int i = 0; i < len; i++){
      Local<Value> argv[argc] = arr -> Get(i);
      cb->Call(Null(isolate), argc, argv);
    }
    args.GetReturnValue().Set(arr);
  }

  void Map(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    HandleScope scope(isolate);
    Local<Value> temp;

    Local<Array> arr = Local<Array>::Cast(args[0]);
    int len = arr -> Length();
    Local<Array> res = Array::New(isolate,len);
    Local<Function> cb = Local<Function>::Cast(args[1]);
    const unsigned argc = 1;
    for(int i = 0; i < len; i++){
      Local<Value> argv[argc] = arr -> Get(i);
      temp = cb->Call(Null(isolate), argc, argv);
      res -> Set(i, temp);
    }
    args.GetReturnValue().Set(res);
  }

  void init(v8::Local<v8::Object> exports) {
    NODE_SET_METHOD(exports, "each", Each);
    NODE_SET_METHOD(exports, "map", Map);
  }

  NODE_MODULE(addon, init)
}
