#include <node.h>

namespace test {

  using namespace v8;

  void Sum(const FunctionCallbackInfo<Value>& args) {
    Isolate* isolate = args.GetIsolate();
    int v = 0;
    for (int i = 0; i < args.Length(); i++) {
      if (!args[i]->IsNumber()) {
        isolate->ThrowException(Exception::TypeError(String::NewFromUtf8(isolate, "Wrong arguments")));
        return;
      }
      v = v + args[i]->NumberValue();
    }
    Local<Value> num = Number::New(isolate, v);
    args.GetReturnValue().Set(num);
  }

  void init(v8::Local<v8::Object> exports) {
    NODE_SET_METHOD(exports, "sum", Sum);
  }

  NODE_MODULE(addon, init)
}
